// ПРОВЕРКА СКРОЛЛА
function checkOffset() {
    if (pageYOffset > 0) {
        $('.up').fadeIn(125);
    } else {
        $('.up').fadeOut(125);
    }
}

var current = 1;
var count = function () {
    return $('div.active .main-slider-element').length;
};

var interval = window.setInterval(sliderNext, 5000);

function sliderNext() {
    window.clearInterval(interval);
    interval = window.setInterval(sliderNext, 5000);
    $('.main-slider div.active .main-slider-element:nth-of-type(' + current + ')').removeClass('showing');
    if (current !== count()) {
        current++;
    }
    else {
        current = 1;
    }
    $('.main-slider div.active .main-slider-element:nth-of-type(' + current + ')').addClass('showing');
}

function sliderPrevious() {
    window.clearInterval(interval);
    interval = window.setInterval(sliderNext, 5000);
    $('.main-slider div.active .main-slider-element:nth-of-type(' + current + ')').removeClass('showing');
    if (current !== 1) {
        current--;
    }
    else {
        current = count();
    }
    $('.main-slider div.active .main-slider-element:nth-of-type(' + current + ')').addClass('showing');
}


// ПРИ ЗАГРУЗКЕ СТРАНИЦЫ
$(document).ready(function () {

    $(' div.active img.main-slider-element:first-of-type').addClass('showing');
    $('button.main-slider-next').on('click', sliderNext);
    $('button.main-slider-previous').on('click', sliderPrevious);

    // ВЫЗОВ ФУНКЦИИ
    checkOffset();

    // ВЫЗОВ ФУНКЦИИ ПРИ СКРОЛЛЕ
    $(window).scroll(function () {
        checkOffset();
    });

    // МАСКА ТЕЛЕФОНА
    $(function () {
        $("#phone").mask("8(999) 999-9999");
    });

    // ПЛАВНЫЙ СКРОЛЛ МЕНЮ
    $('nav').find('a').on('click', function () {
        var destination = $(this).attr('name');
        $('html, body').animate({
            scrollTop: $('.' + destination).offset().top - 100
        }, 500)
    });

    // НАВЕРХ
    $('.up').on('click', function () {
        $('html, body').animate({
            scrollTop: 0
        }, 500)
    });

    // ОТКРЫТИЕ ОКНА
    $('div.gallery a').on('click', function () {
        $('div.choice, div.background').fadeIn(125);
    });

    // ЗАКРЫТЬ ОКНО
    $('div.background').on('click', function () {
        $('div.choice, div.background').fadeOut(125);
    });

    // ВЫБОР ПУНКТА
    $('div.choice li').on('click', function () {

        // Сброс таймер
        window.clearInterval(interval);
        interval = window.setInterval(sliderNext, 5000);

        // Переменные
        var text = $(this)[0].innerHTML;
        var index = $(this).index();
        var wrappers = $('div.main-slider-wrapper');
        var newWrapper = wrappers[index];

        wrappers.removeClass('active');
        $(newWrapper).addClass('active');
        $('img.main-slider-element').removeClass('showing');
        $($('div.active img.main-slider-element')[0]).addClass('showing');
        $('div.gallery a').text(text);
        $('div.choice, div.background').fadeOut(125);
    });

    // ОТПРАВКА ФОРМЫ ОБРАТНОГО ЗВОНКА
    $('.callback').submit(function (event) {
        event.preventDefault();
        $('.callback-send').val('Секунду...');
        var name = $(this).find('#name').val();
        var phone = $(this).find('#phone').val();
        $.ajax({
            method: 'POST',
            url: 'php/callback.php',
            data: {name: name, phone: phone}
        }).done(function (result) {
            $('.callback-send').val(result);
        }).fail(function () {
            $('.callback-send').val('Произошла ошибка');
        });
    });

});
